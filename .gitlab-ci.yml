---


# This file is part of racket-ebuild.

# racket-ebuild is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# racket-ebuild is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License
# SPDX-License-Identifier: GPL-3.0-only


variables:
  GIT_SUBMODULE_STRATEGY: recursive
  TERM: dumb


stages:
  - build
  - test
  - bin
  - pages


image: racket/racket:8.5-full


before_script:
  - apt-get update
  - apt-get install -y make sqlite3

  - make install


build:
  stage: build
  only:
    changes:
      - ".gitlab-ci.yml"
      - "scripts/*.rkt"
      - "src/**/*"

  script:
    - make compile


shellcheck:
  stage: test
  only:
    changes:
      - ".gitlab-ci.yml"
      - "src/ebuild-lib/ebuild/version.rkt"
      - "scripts/*.sh"

  image: alpine:3.15.0

  before_script:
    - apk update
    - apk add make shellcheck

  script:
    - make shellcheck


test:
  stage: test
  only:
    changes:
      - ".gitlab-ci.yml"
      - "src/**/*"

  script:
    - make test


test-deps:
  stage: test
  only:
    changes:
      - ".gitlab-ci.yml"
      - "src/**/*"

  script:
    - make test-deps


test-collector2:
  stage: test
  needs: ["test"]
  only:
    changes:
      - ".gitlab-ci.yml"
      - "src/ebuild-lib/**/*.rkt"

  script:
    - make test-collector2


bin:
  stage: bin
  needs: ["test"]
  only:
    changes:
      - ".gitlab-ci.yml"
      - "scripts/exes.rkt"
      - "src/ebuild-lib/ebuild/version.rkt"
      - "src/ebuild-tools/ebuild/tools/**/*"

  script:
    - make bin


pages-test-build:
  stage: pages
  needs: ["test"]
  only:
    changes:
      - ".gitlab-ci.yml"
      - "src/ebuild-lib/ebuild/version.rkt"
      - "src/ebuild-doc/ebuild/scribblings/**/*"

  script:
    - apt-get install -y git
    - make docs-public


pages:
  stage: pages
  only:
    refs:
      - master
    changes:
      - ".gitlab-ci.yml"
      - "src/ebuild-lib/ebuild/version.rkt"
      - "src/ebuild-doc/ebuild/scribblings/**/*"

  artifacts:
    expire_in: 1 week
    paths:
      - public

  script:
    - apt-get install -y git
    - make docs-public
