# This file is part of racket-ebuild.

# racket-ebuild is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# racket-ebuild is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with racket-ebuild.	 If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License
# SPDX-License-Identifier: GPL-3.0-only


MAKE        := make
RACKET      := racket
RACO        := raco
SCRIBBLE    := $(RACO) scribble
SH          := sh
RM          := rm
RMDIR       := $(RM) -r

BIN         := $(PWD)/bin
CACHE       := $(PWD)/.cache
DOCS        := $(PWD)/docs
PUBLIC      := $(PWD)/public
SCRIPTS     := $(PWD)/scripts
SRC         := $(PWD)/src
TESTS       := $(PWD)/tests

COLLECTOR2_INSTALL_FLAGS    := --auto --skip-installed
COLLECTOR2_SETUP_FLAGS      := --avoid-main --no-docs --tidy
COLLECTOR2_TEST_FLAGS       := --heartbeat --submodule test --table
COLLECTOR2_OVERLAY          := $(CACHE)/collector2/overlay
COLLECTOR2_RUN_PACKAGE      := ebuild-lib
COLLECTOR2_RUN_FLAGS        := --create --directory $(COLLECTOR2_OVERLAY) --only-package $(COLLECTOR2_RUN_PACKAGE)


.PHONY: all
all: compile


ebuild-make-%:
	$(MAKE) -C $(SRC) DEPS-FLAGS=" --no-pkg-deps " $(*)


.PHONY: clean-ebuild
clean-ebuild: ebuild-make-clean

.PHONY: clean-scripts
clean-scripts:
	if [ -d $(SCRIPTS)/compiled ] ; then $(RMDIR) $(SCRIPTS)/compiled ; fi

.PHONY: clean
clean: clean-bin clean-ebuild clean-public clean-scripts


.PHONY: compile-ebuild
compile-ebuild: ebuild-make-compile

# No .PHONY
scripts/compiled:
	$(RACO) make -v $(SCRIPTS)/*.rkt

.PHONY: compile-scripts
compile-scripts: scripts/compiled

.PHONY: recompile-scripts
recompile-scripts: clean-scripts compile-scripts

.PHONY: compile
compile: recompile-scripts compile-ebuild


.PHONY: install-ebuild
install-ebuild: ebuild-make-install

.PHONY: install
install: install-ebuild


.PHONY: setup-ebuild
setup-ebuild: ebuild-make-setup

.PHONY: setup
setup: setup-ebuild


.PHONY: test-ebuild
test-ebuild: ebuild-make-test


.PHONY: remove-ebuild
remove-ebuild: ebuild-make-remove

.PHONY: remove
remove: remove-ebuild


# No .PHONY
bin: compile-scripts
	$(RACKET) $(SCRIPTS)/exes.rkt

.PHONY: clean-bin
clean-bin:
	if [ -d $(BIN) ] ; then $(RMDIR) $(BIN) ; fi

.PHONY: regen-bin
regen-bin: clean-bin bin


# Do not include shellcheck target as a dependency
# No .PHONY
.cache:
	mkdir -p $(CACHE)

.PHONY: shellcheck
shellcheck:
	find $(PWD) -type f -name "*.sh" -exec shellcheck {} +

.PHONY: test-unit
test-unit: .cache
	TMPDIR=$(PWD)/.cache $(RACKET) $(TESTS)/test.rkt --unit

.PHONY: test-integration
test-integration: .cache
	TMPDIR=$(PWD)/.cache $(RACKET) $(TESTS)/test.rkt --integration

.PHONY: test-deps
test-deps:
	$(MAKE) -C $(SRC) setup

.PHONY: clean-collector-overlay
clean-collector-overlay:
	if [ -d $(COLLECTOR2_OVERLAY) ] ; then $(RMDIR) $(COLLECTOR2_OVERLAY) ; fi

.PHONY: test-collector2
test-collector2: clean-collector-overlay
	$(RACO) pkg install $(COLLECTOR2_INSTALL_FLAGS) collector2
	$(RACO) setup $(COLLECTOR2_SETUP_FLAGS) --pkgs collector2-lib
	$(RACO) test $(COLLECTOR2_TEST_FLAGS) --package collector2-test
	$(RACKET) --lib collector2 -- $(COLLECTOR2_RUN_FLAGS)

.PHONY: test
test: test-unit test-integration

.PHONY: test-all
test-all: test-deps test test-collector2


.PHONY: completion
completion:
	$(SH) $(SCRIPTS)/completion.sh


# No .PHONY
public:
	mkdir -p $(PUBLIC)

.PHONY: docs-html
docs-html: public
	cd $(DOCS) && $(SCRIBBLE) ++main-xref-in --dest $(PWD) \
		--dest-name public --htmls --quiet $(DOCS)/scribblings/main.scrbl

.PHONY: docs-latex
docs-latex: public
	$(RACKET) $(SCRIPTS)/doc.rkt latex

.PHONY: docs-markdown
docs-markdown: public
	$(RACKET) $(SCRIPTS)/doc.rkt markdown

.PHONY: docs-pdf
docs-pdf: public
	$(RACKET) $(SCRIPTS)/doc.rkt pdf

.PHONY: docs-text
docs-text: public
	$(RACKET) $(SCRIPTS)/doc.rkt text

.PHONY: docs-public
docs-public: docs-html

.PHONY: docs-all
docs-all: docs-html docs-latex docs-markdown docs-pdf docs-text

.PHONY: clean-public
clean-public:
	if [ -d $(PUBLIC) ] ; then $(RMDIR) $(PUBLIC) ; fi

.PHONY: regen-public
regen-public: clean-public docs-public
