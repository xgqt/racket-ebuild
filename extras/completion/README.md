# Completions


## ZSH

Copy the completions into a directory on your `fpath`,
or execute the following commands to make the completions available
without installation.

``` shell
fpath=( $(pwd)/extras/completion/zsh ${fpath} )
compinit -u
```
