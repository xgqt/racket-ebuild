#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang ebuild


(module+ test
  (require rackunit)

  (check-equal?  (list-as-variable "Z" '())             "Z=")
  (check-equal?  (list-as-variable "Z" '(0 1 2))        "Z=\"0 1 2\"")
  (check-equal?  (list-as-variable "Z" '(z z z))        "Z=\"z z z\"")
  (check-equal?  (list-as-variable "Z" '("z" "z" "z"))  "Z=\"z z z\"")
  (check-equal?  (list-as-variable "Z" '(0 z "z"))      "Z=\"0 z z\"")

  (check-equal?  (as-variable "Z")              "Z=")
  (check-equal?  (as-variable "Z" 0)            "Z=0")
  (check-equal?  (as-variable "Z" 'v)           "Z=v")
  (check-equal?  (as-variable "Z" "z z z")      "Z=\"z z z\"")
  (check-equal?  (as-variable "Z" 0 1 2)        "Z=\"0 1 2\"")
  (check-equal?  (as-variable "Z" 'z 'z 'z)     "Z=\"z z z\"")
  (check-equal?  (as-variable "Z" "z" "z" "z")  "Z=\"z z z\"")
  (check-equal?  (as-variable "Z" 0 'z "z")     "Z=\"0 z z\"")

  (define Zs "ZZZ")
  (check-equal?  (make-variable Zs)   "Zs=\"ZZZ\"")
  (check-equal?  (make-variable "Z")  "Z=\"Z\"")
  (check-equal?  (make-variable 'Z)   "Z=Z")


  (check-equal?  (list-as-array-variable "Y" '())             "Y=()")
  (check-equal?  (list-as-array-variable "Y" '(0 1 2))        "Y=(0 1 2)")
  (check-equal?  (list-as-array-variable "Y" '(y y y))        "Y=(y y y)")
  (check-equal?  (list-as-array-variable "Y" '("y" "y" "y"))  "Y=(\"y\" \"y\" \"y\")")
  (check-equal?  (list-as-array-variable "Y" '(0 y "y"))      "Y=(0 y \"y\")")

  (check-equal?  (as-array-variable "Y")              "Y=()")
  (check-equal?  (as-array-variable "Y" 0)            "Y=(0)")
  (check-equal?  (as-array-variable "Y" 'v)           "Y=(v)")
  (check-equal?  (as-array-variable "Y" "y y y")      "Y=(\"y y y\")")
  (check-equal?  (as-array-variable "Y" 0 1 2)        "Y=(0 1 2)")
  (check-equal?  (as-array-variable "Y" 'y 'y 'y)     "Y=(y y y)")
  (check-equal?  (as-array-variable "Y" "y" "y" "y")  "Y=(\"y\" \"y\" \"y\")")
  (check-equal?  (as-array-variable "Y" 0 'y "y")     "Y=(0 y \"y\")")

  (define Ys '("Y" "Y" "Y"))
  (check-equal?  (make-array-variable Ys)  "Ys=(\"Y\" \"Y\" \"Y\")")
  (check-equal?  (as-array-variable "Y" "Y")  "Y=(\"Y\")")
  (check-equal?  (as-array-variable "Y" 'Y)  "Y=(Y)")


  (define F #f)
  (check-equal?  (make-variable F)  #f)
  (check-equal?  (make-array-variable F)  #f))
