#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang racket/base


(module+ test
  (require
   rackunit
   xml
   ebuild/metadata
   )

  (define-values (c0 c1 c2 c3 c4 c5)
    (values
     (slots #f #f #f)
     (slots "en" '() #f)
     (slots "en" #f "ABI")
     (slots "en" (list (slot "1" "lib.so.1")) #f)
     (slots "en" (list (slot "1" "lib.so.1") (slot "2" "lib.so.2")) #f)
     (slots "en" (list (slot "1" "lib.so.1") (slot "2" "lib.so.2")) "ABI")
     )
    )
  (define lc (list c0 c1 c2 c3 c4 c5))

  (for ([c lc])
    (check-not-false  (xexpr->xml (slots->xexpr c)))
    )
  )
