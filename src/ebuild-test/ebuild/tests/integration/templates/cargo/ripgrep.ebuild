# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

CRATES="
	aho-corasick-0.7.10
	atty-0.2.14
	autocfg-1.0.0"

inherit bash-completion-r1 cargo

DESCRIPTION="Package for ripgrep test"
HOMEPAGE="https://github.com/BurntSushi/ripgrep"
SRC_URI="https://github.com/BurntSushi/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz
	$(cargo_crate_uris ${CRATES})"

LICENSE="Apache-2.0 BSD-2 Boost-1.0 || ( MIT Unlicense )"
SLOT="0"
KEYWORDS="~amd64"
