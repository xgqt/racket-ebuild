#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang typed/racket/base

(define-type XExpr
  (U Number String Symbol
     (Pair Symbol (Pair (Listof (List Symbol String)) (Listof XExpr)))
     (Pair Symbol (Listof XExpr))))

(provide XExpr)

(require/typed/provide xml
  [#:struct location
   ([line        : (Option Exact-Nonnegative-Integer)]
    [char        : (Option Exact-Nonnegative-Integer)]
    [offset      : Exact-Nonnegative-Integer])]
  [#:struct source
   ([start       : location]
    [stop        : location])]
  [#:struct (attribute source)
   ([name        : Symbol]
    [value       : Any])]
  [#:struct (element source)
   ([name        : Symbol]
    [attributes  : (Listof attribute)]
    [content     : (Listof Any)])]
  [#:struct (p-i source)
   ([target-name : Symbol]
    [instruction : String])]
  [#:struct external-dtd
   ([system      : String])]
  [#:struct (external-dtd/system external-dtd)
   ()]
  [#:struct document-type
   ([name        : Symbol]
    [external    : external-dtd]
    [inlined     : False])]
  [#:struct prolog
   ([misc        : (Listof p-i)]
    [dtd         : (Option document-type)]
    [misc2       : (Listof p-i)])]
  [#:struct document
   ([prolog      : prolog]
    [element     : element]
    [misc        : (Listof p-i)])]
  [xexpr->xml      (-> XExpr element)])
