#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, package-version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang scribble/manual

@(require (only-in scribble/bnf nonterm)
          "common-options.rkt"
          (for-label racket
                     ebuild/tools/githost2metadata))


@title[#:tag "ebuild-tools-githost2metadata"]{GitHost2Metadata}


@defmodule[ebuild/tools/githost2metadata]


@section[#:tag "ebuild-tools-githost2metadata-about"]{About}

Create a PMS package metadata file from Git hosting service repository.


@section[#:tag "ebuild-tools-githost2metadata-cli"]{Console usage}

@itemlist[
 @item{
  @DFlag{c} or @DFlag{create}
  --- create (save) the metadata
 }
 @item{
  @DFlag{s} or @DFlag{show}
  --- show (display) the metadata
 }

 @item{
  @DFlag{d} @nonterm{directory-path}
  or @DFlag{dir} @nonterm{directory-path}
  --- directory where the created metadata file should be saved
 }

 @verbose-flag
 @quiet-flag

 @help-flag
 @version-flag
 ]

githost2metadata also expects two arguments given last in the command
invocation.
First is the git hosting domain (e.g.: github.com, gitlab.com, ...).
Second is the repository path (e.g.: xgqt/racket-ebuild, gentoo/gentoo, ...).
