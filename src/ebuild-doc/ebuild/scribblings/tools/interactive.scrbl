#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, package-version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang scribble/manual

@(require "common-options.rkt"
          (for-label racket
                     ebuild/tools/interactive))


@title[#:tag "ebuild-tools-interactive"]{Interactive}


@defmodule[ebuild/tools/interactive]


@section[#:tag "ebuild-tools-interactive-about"]{About}

Interactively creates packages.


@section[#:tag "ebuild-tools-interactive-cli"]{Console usage}

@itemlist[
 @help-flag
 @version-flag
 ]
