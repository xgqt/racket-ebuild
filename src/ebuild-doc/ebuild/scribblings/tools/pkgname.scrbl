#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, package-version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang scribble/manual

@(require "common-options.rkt"
          (for-label racket
                     ebuild/tools/pkgname))


@title[#:tag "ebuild-tools-pkgname"]{PKGName}


@defmodule[ebuild/tools/pkgname]


@section[#:tag "ebuild-tools-pkgname-about"]{About}

Shows package name for a given directory.


@section[#:tag "ebuild-tools-pkgname-cli"]{Console usage}

@itemlist[
 @item{
  @Flag{c} or @DFlag{no-category}
  --- do not show package categories
 }

 @item{
  @Flag{D} or @DFlag{debug}
  --- run with debugging turned on
 }

 @help-flag
 @version-flag
 ]

Also takes any number of of arguments not followed by flags
that specify system paths to be passed to "pkgname".
