#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, package-version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang scribble/manual

@(require (only-in scribble/bnf nonterm)
          "common-options.rkt"
          (for-label racket
                     ebuild/tools/modify-metadata))


@title[#:tag "ebuild-tools-modify-metadata"]{Modify metadata}


@defmodule[ebuild/tools/modify-metadata]


@section[#:tag "ebuild-tools-modify-metadata-about"]{About}

Modify metadata.xml files.


@section[#:tag "ebuild-tools-modify-metadata-cli"]{Console usage}

@itemlist[
 @item{
  @Flag{a} @nonterm{name} @nonterm{contents} or
  @DFlag{add} @nonterm{name} @nonterm{contents}
  --- add a field of name with contents
 }
 @item{
  @Flag{r} @nonterm{name} or @DFlag{remove} @nonterm{name}
  --- remove a field with name
 }

 @help-flag
 @version-flag
 ]
