#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, package-version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang scribble/manual

@(require (only-in scribble/bnf nonterm)
          "common-options.rkt"
          (for-label racket
                     ebuild/tools/url2pkg))


@title[#:tag "ebuild-tools-url2pkg"]{UL2PKG}


@defmodule[ebuild/tools/url2pkg]


@section[#:tag "ebuild-tools-url2pkg-about"]{About}

Creates a package from given URL based on different heuristic tactics.

Quality: very experimental.


@section[#:tag "ebuild-tools-url2pkg-cli"]{Console usage}

@itemlist[
 @item{
  @DFlag{pn} @nonterm{package-name}
  or @DFlag{package-name} @nonterm{package-name}
  --- package name
 }
 @item{
  @DFlag{pv} @nonterm{package-version}
  or @DFlag{package-version} @nonterm{package-version}
  --- package version
 }

 @item{
  @DFlag{save}
  --- save generated package
 }
 @item{
  @DFlag{show}
  --- show generated package
 }

 @help-flag
 @version-flag
 ]
