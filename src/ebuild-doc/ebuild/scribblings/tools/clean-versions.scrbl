#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, package-version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang scribble/manual

@(require (only-in scribble/bnf nonterm)
          "common-options.rkt"
          (for-label racket
                     ebuild/tools/clean-versions))


@title[#:tag "ebuild-tools-clean-versions"]{Clean Versions}


@defmodule[ebuild/tools/clean-versions]


@section[#:tag "ebuild-tools-clean-versions-about"]{About}

Cleans up old versions of ebuilds from a repository.


@section[#:tag "ebuild-tools-clean-versions-cli"]{Console usage}

@itemlist[
 @item{
  @Flag{m} @nonterm{number} or @DFlag{max} @nonterm{number}
  --- maximum number of ebuilds to keep
 }
 @item{
  @Flag{r} @nonterm{path} or @DFlag{repository} @nonterm{path}
  --- directory path to ebuild repository
 }

 @item{
  @Flag{s} or @DFlag{simulate}
  --- simulate, show only what would be deleted
 }
 @item{
  @Flag{n} or @DFlag{no-simulate}
  --- create ebuilds in a directory
  (defaults to current unless a directory is specified by @DFlag{repository})
 }

 @quiet-flag
 @verbose-flag

 @help-flag
 @version-flag
 ]
