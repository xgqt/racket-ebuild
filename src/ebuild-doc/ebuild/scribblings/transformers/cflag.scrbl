#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, package-version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang scribble/manual

@(require scribble/example
          (for-label racket
                     ebuild
                     ebuild/transformers/cflag))


@(define doc-eval
   (make-base-eval '(require ebuild ebuild/transformers/cflag)))


@title[#:tag "ebuild-transformers-cflag"]{CFlag Transformers}

@defmodule[ebuild/transformers/cflag]


@defproc[
 (string->cflag [str string?]) (listof (or/c cflag? string?))
 ]{
 Converts a string back to @racket[cflag]s.

 @examples[
 #:eval doc-eval
 (string->cflag "X? ( x11-libs/libX11 ) qt5? ( dev-qt/qtsvg dev-qt/qtwidgets )")
 ]
}
