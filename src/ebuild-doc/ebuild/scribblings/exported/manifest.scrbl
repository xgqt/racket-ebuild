#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, package-version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang scribble/manual

@(require scribble/example
          (for-label racket
                     ebuild/manifest))


@(define doc-eval
   (make-base-eval '(require ebuild/manifest)))


@title[#:tag "ebuild-exported-manifest"]{Manifest Functions}

@defmodule[ebuild/manifest]

This library does not use classes but rather structs.
It is implemented in Typed Racket.

This library allows reading and creating Manifests.

Manifest files hold digests and size data for every file used by the package.
More info about Manifests can be found in the
@link["https://devmanual.gentoo.org/general-concepts/manifest/#thin-and-thick-manifests"
      "Gentoo Developer manual"].


@section[#:tag "ebuild-exported-manifest-checksum"]{Checksum}

@defstruct[
 checksum
 ([type string?] [hash string?])
 ]{
 @examples[
 #:eval doc-eval
 (checksum "BLAKE2B" "aa81a1a")
 ]
}

@defproc[
 (list->checksums [lst (listof string?)]) (listof checksum?)
 ]{
 @examples[
 #:eval doc-eval
 (list->checksums '("BLAKE2B" "aa81a1a" "SHA521" "b6095d4"))
 ]
}

@defproc[
 (checksum->string [cs checksum?]) string?
 ]{
 @examples[
 #:eval doc-eval
 (checksum->string (checksum "BLAKE2B" "aa81a1a"))
 ]
}


@section[#:tag "ebuild-exported-manifest-dist"]{Dist}

@defstruct[
 dist
 ([type         (or/c 'dist 'ebuild 'misc)]
  [file         string?]
  [size         integer?]
  [checksums    (listof checksum?)])
 ]{
 @examples[
 #:eval doc-eval
 (dist 'dist "package-source.tar.gz" 100 (list (checksum "BLAKE2B" "aa81a1a")))
 ]
}

@defproc[
 (string->dist-type [str string?]) (or/c 'dist 'ebuild 'misc)
 ]{
 @examples[
 #:eval doc-eval
 (string->dist-type "EBUILD")
 ]
}

@defproc[
 (dist-type->string [dist-type (or/c 'dist 'ebuild 'misc)]) string?
 ]{
 @examples[
 #:eval doc-eval
 (dist-type->string 'misc)
 ]
}

@defproc[
 (string->dist [str string?]) dist?
 ]{
 @examples[
 #:eval doc-eval
 (string->dist "DIST gcc-9.4.0.tar.xz 72411232 BLAKE2B 4bb000d SHA512 dfd3500")
 ]
}

@defproc[
 (dist->string [dst dist?]) string?
 ]{
}


@section[#:tag "ebuild-exported-manifest-manifest"]{Manifest}

@defstruct[
 manifest
 ([dists (listof dist?)])
 ]{
}

@defproc[
 (read-manifest [path path-string?]) manifest?
 ]{
}

@defproc[
 (write-manifest [mnfst manifest?] [out output-port? (current-output-port)])
 void?
 ]{
}

@defproc[
 (save-manifest [mnfst manifest?] [path path-string?]) void?
 ]{
}
