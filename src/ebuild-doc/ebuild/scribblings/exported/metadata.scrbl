#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, package-version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang scribble/manual

@(require scribble/example
          (for-label racket
                     xml
                     ebuild))


@(define metadata-eval
   (make-base-eval '(require xml ebuild/metadata)))


@declare-exporting[ebuild/metadata]


@title[#:tag "ebuild-exported-metadata"]{Metadata Functions}


The @racket[structname->xexpr] procedures are primarily used internally
but are also exported because sometimes they can be handy.


@section{XML tags}

@defparam[
 metadata-empty-tags list list?
 #:value (list 'stabilize-allarches)
 ]{
 Parameter that determines shorthanded tags @racket[list] passed to
 @racket[empty-tag-shorthand].
}


@section{localized}

@defstruct[
 localized
 ([lang (or/c #f string? symbol?)]
  [data string?])
 ]{
 "Intermediate" strcture inherited by in @racket[longdescription]
 and @racket[doc].
}

@defproc[
 (localized->xexpr
  [lo           localized?]
  [element-name symbol?])
 xexpr?
 ]{
 Converts @racket[lo localized] struct to a x-expression,
 where the x-expression tag is @racket[element-name].
}


@section{longdescription}

@defstruct[
 (longdescription localized) ()
 ]{
}

@defproc[
 (longdescription->xexpr
  [ld longdescription?])
 xexpr?
 ]{
 Converts @racket[ld longdescription] struct to a x-expression.
}

@examples[
 #:eval metadata-eval
 (define my-longdescription
   (longdescription
    "en"
    #<<EOF
This is some package providing some very important function,
  everybody probably needs it, of course it is written in the best
  programming language starting with letter R ;D
EOF
    ))
 (display-xml/content (xexpr->xml (longdescription->xexpr my-longdescription)))
 ]


@section{maintainer}

@defstruct[
 maintainer
 ([type        (or/c 'person 'project 'unknown)]
  [proxied     (or/c #f 'yes 'no 'proxy)]
  [email       string?]
  [name        (or/c #f string?)]
  [description (or/c #f string?)])
 ]{
}

@defproc[
 (maintainer->xexpr
  [maint maintainer?])
 xexpr?
 ]{
 Converts @racket[maint maintainer] struct to a x-expression.
}

@examples[
 #:eval metadata-eval
 (define my-maintainer
   (maintainer 'person #f "asd@asd.asd" "A.S.D." #f))
 (display-xml/content (xexpr->xml (maintainer->xexpr my-maintainer)))
 ]


@section{slots}

@defstruct[
 slot
 ([name string?]
  [data string?])
 ]{
}

@defstruct[
 slots
 ([lang     (or/c #f string? symbol?)]
  [slotlist (or/c #f (listof slot?))]
  [subslots (or/c #f string?)])
 ]{
}

@defproc[
 (slots->xexpr
  [ss slots?])
 xexpr?
 ]{
 Converts @racket[ss slots] struct to a x-expression.
}

@examples[
 #:eval metadata-eval
 (define my-slots
   (slots #f (list (slot "1.9" "Provides libmypkg19.so.0")) #f))
 (display-xml/content (xexpr->xml (slots->xexpr my-slots)))
 ]


@section{upstream}

@defstruct[
 upstreammaintainer
 ([status (or/c #f 'active 'inactive 'unknown)]
  [email  (or/c #f string?)]
  [name   string?])
 ]{
}

@defstruct[
 remote-id
 ([type (or/c
         'bitbucket
         'cpan 'cpan-module 'cpe 'cran 'ctan
         'gentoo 'github 'gitlab 'google-code
         'heptapod
         'launchpad
         'osdn
         'pear 'pecl 'pypi
         'rubygems
         'sourceforge
         'vim
         )]
  [tracker string?])
 ]{
}

@defstruct[
 (doc localized) ()
 ]{
}

@defproc[
 (docs->xexpr
  [v (or/c #f string? (listof doc?))])
 xexpr?
 ]{
 Given a @racket[string] or @racket[list] of @racket[doc] produces
 produces @racket[list] of x-expressions.
 Given @racket[false] produces a empty list.
}

@defstruct[
 upstream
 ([maintainers (listof upstreammaintainer?)]
  [changelog   (or/c #f string?)]
  [doc         (or/c #f string? (listof doc?))]
  [bugs-to     (or/c #f string?)]
  [remote-ids  (listof remote-id?)])
 ]{
}

@defproc[
 (upstream->xexpr
  [up upstream?])
 xexpr?
 ]{
 Converts @racket[up upstream] struct to a x-expression.
}

@examples[
 #:eval metadata-eval
 (define my-upstream
   (upstream (list) #f #f #f (list (remote-id 'gitlab "asd/asd"))))
 (display-xml/content (xexpr->xml (upstream->xexpr my-upstream)))
 (set-upstream-doc! my-upstream "https://asd.asd/docs.html")
 (display-xml/content (xexpr->xml (upstream->xexpr my-upstream)))
 (set-upstream-doc! my-upstream (list (doc "en" "https://asd.asd/doc.html")
                                      (doc "pl" "https://asd.asd/dok.html")))
 (display-xml/content (xexpr->xml (upstream->xexpr my-upstream)))
 ]


@section{use}

@defstruct[
 uflag
 ([name string?]
  [data (or/c #f string?)])
 ]{
}

@defstruct[
 use
 ([lang  (or/c #f string? symbol?)]
  [flags (listof uflag?)])
 ]{
}

@defproc[
 (use->xexpr
  [us use?])
 xexpr?
 ]{
 Converts @racket[us use] struct to a x-expression.
}

@examples[
 #:eval metadata-eval
 (define my-use
   (use #f (list (uflag "racket" "Build using dev-scheme/racket"))))
 (display-xml/content (xexpr->xml (use->xexpr my-use)))
 ]
