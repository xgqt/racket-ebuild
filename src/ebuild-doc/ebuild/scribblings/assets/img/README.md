# Racket-Ebuild-Logo

"racket-ebuild-logo.svg" is based on
Gentoo-in-a-package (CC-BY-SA/2.5) by Michał Górny
and Racket logo (MIT)
and licensed under CC-BY-SA/2.5
