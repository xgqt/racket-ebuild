#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, package-version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang scribble/manual

@(require scribble/example
          (for-label racket
                     racket/class
                     (only-in racket/date current-date)
                     ebuild))


@(define ebuild-eval
   (make-base-eval '(require racket/class ebuild/ebuild)))


@title[#:tag "ebuild-classes-ebuild"]{Ebuild Class}


@defmodule[ebuild/ebuild]

@defclass[
 ebuild% object% (printable<%>)
 ]{
 Ebuild class.

 For creating package
 @link["https://devmanual.gentoo.org/ebuild-writing/file-format/" "ebuild"]
 files (@filepath{package.ebuild}).
 @defconstructor[
 ([year                  integer? (date-year (current-date))]
  [EAPI                  integer? 8]
  [custom                (listof (or/c (-> any) string?)) '()]
  [inherits              (listof string?) '()]
  [DESCRIPTION           string? "package"]
  [HOMEPAGE              string? "https://wiki.gentoo.org/wiki/No_homepage"]
  [SRC_URI               (listof src-uri?) '()]
  [S                     (or/c #f string?) #f]
  [LICENSE               string? "all-rights-reserved"]
  [SLOT                  string? "0"]
  [KEYWORDS              (listof string?) '("~amd64")]
  [IUSE                  (listof string?) '()]
  [REQUIRED_USE          (listof cflag?)  '()]
  [RESTRICT              (listof (or/c cflag? string?)) '()]
  [COMMON_DEPEND         (listof (or/c cflag? string?)) '()]
  [RDEPEND               (listof (or/c cflag? string?)) '()]
  [DEPEND                (listof (or/c cflag? string?)) '()]
  [BDEPEND               (listof (or/c cflag? string?)) '()]
  [IDEPEND               (listof (or/c cflag? string?)) '()]
  [PDEPEND               (listof (or/c cflag? string?)) '()]
  [DOCS                  (listof string?) '()]
  [PATCHES               (listof string?) '()]
  [QA_PREBUILT           (listof string?) '()]
  [QA_TEXTRELS           (listof string?) '()]
  [QA_EXECSTACK          (listof string?) '()]
  [QA_WX_LOAD            (listof string?) '()]
  [QA_FLAGS_IGNORED      (listof string?) '()]
  [QA_MULTILIB_PATHS     (listof string?) '()]
  [QA_PRESTRIPPED        (listof string?) '()]
  [QA_SONAME             (listof string?) '()]
  [QA_SONAME_NO_SYMLINK  (listof string?) '()]
  [QA_AM_MAINTAINER_MODE (listof string?) '()]
  [QA_CONFIGURE_OPTIONS  (listof string?) '()]
  [QA_DT_NEEDED          (listof string?) '()]
  [QA_DESKTOP_FILE       (listof string?) '()]
  [body                  (listof (or/c (-> any) string?)) '()])
 ]{
 }

 @defmethod[
 (create-list)
 list?
 ]{
  Creates a @racket[list] that contains
  @racket[string]s or @racket[false] as elements.
  The elaments of the list are created using the "unroll" functions
  which are not exposed to the user.

  Also, concatenates values that
  @racket[custom] and @racket[body] arguments return.
 }

 @defmethod[
 (create)
 string?
 ]{
  Takes non-@racket[false] elemets of the @racket[list] created by
  @method[ebuild% create-list] method and turns them into one
  @racket[string] ready to be written into a file
  (by @method[ebuild% save] for example).
  }

 @defmethod[
 (save
  [name  string?]
  [pth   path-string? (current-directory)])
 void
 ]{
  Creates a file named @racket[name]
  in the given location @racket[pth] (or current directory).
  Internally uses the interfece implemented by this object's
  @racket[printable<%>] to dispaly this to file.
 }

 @defmethod[
 (append!
  [sym  symbol?]
  [lst  list?])
 void
 ]{
  Uses @racket[dynamic-get-field] and @racket[dynamic-set-field!].
  Sets the field-name represented by @racket[sym] symbol of this object
  to that field appended with @racket[lst] @racket[list].
  }

 @defmethod[
 (concat!
  [sym  symbol?]
  [v    any/c])
 void
 ]{
  Like @method[ebuild% append!]
  but a list is automatically generated from @racket[v].
 }

 @examples[
 #:eval ebuild-eval
 (define my-ebuild
   (new ebuild%
        [IUSE (list "debug" "test")]
        [RESTRICT (list (cflag "test?" (list "debug")))]))
 (display my-ebuild)
 ]

}
