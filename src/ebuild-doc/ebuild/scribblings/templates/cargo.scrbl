#!/usr/bin/env racket


;; This file is part of racket-ebuild.

;; racket-ebuild is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, package-version 3.

;; racket-ebuild is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ebuild.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang scribble/manual

@(require scribble/example
          (for-label racket
                     ebuild
                     ebuild/templates/cargo))


@(define cargo-eval
   (make-base-eval
    '(require racket/class ebuild/ebuild ebuild/templates/cargo)))


@title[#:tag "ebuild-templates-cargo"]{Rust's Cargo}


@defmodule[ebuild/templates/cargo]


@defmixin[ebuild-cargo-mixin (ebuild%) ()]{}


@defclass[
 ebuild-cargo% ebuild% ()
 ]{
 Pre-made class extending @racket[ebuild%] for writing ebuilds using the
 @link["https://gitweb.gentoo.org/repo/gentoo.git/tree/eclass/cargo.eclass"]{
  cargo.eclass}.

 When creating a @racket[ebuild-cargo%] object
 following values are automatically added to fields:
 @itemlist[@item{"cargo" to "inherit"ed eclasses}
           @item{"$(cargo_crate_uris ${CRATES})" to SRC_URI}
           @item{CRATES to a CRATES variable}
           ]

 @defconstructor[
 (
  [CRATES  (listof string?)  '()]
  )
 ]{
 }

 @examples[
 #:eval cargo-eval
 (define my-ebuild
   (new ebuild-cargo% [CRATES '("crate1" "crate2" "crate3")]))
 (display my-ebuild)
 ]

}
